"""
The ion load experiment demonstrates how to create a frontend for the ion load service.
"""

from dax_example.system import *


class IonLoadExperiment(DaxExampleSystem, Experiment):
    """Ion load service"""

    def build(self):
        # Call super
        super(IonLoadExperiment, self).build()

        # Add general arguments
        self._num_ions = self.get_argument(
            'Number of ions',
            NumberValue(1, min=1, max=self.detection.MAX_NUM_CHANNELS, step=1, ndecimals=0)
        )

    def run(self):
        # DAX init
        self.dax_init()

        # Call the ion load service
        self.ion_load.load_ions(num_ions=self._num_ions)
