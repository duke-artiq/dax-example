"""
DAX contains data-processing modules that can be used as part of of your DAX system.
These data-processing modules are able to collect, process, and plot data for specific use-cases.
The time resolved service experiment demonstrates how the time resolved data-processor
(part of the time resolved service) is used.
"""

from dax_example.system import *

from dax.modules.time_resolved_context import TimeResolvedAnalyzer


class TimeResolvedExperiment(DaxExampleSystem, Experiment):
    """Time resolved service"""

    def build(self):
        # Call super
        super(TimeResolvedExperiment, self).build()

        # Get arguments
        self._num_bins = self.get_argument('Num bins', NumberValue(128, min=1, step=1, ndecimals=0))
        self._bin_width = self.get_argument('Bin width',
                                            NumberValue(100 * us, unit='us', min=0 * us, step=5 * us))
        self._bin_spacing = self.get_argument('Bin spacing',
                                              NumberValue(0 * us, unit='us', min=0 * us, step=1 * us))
        self._plot_trace = self.get_argument('Plot trace', BooleanValue(True))
        self.update_kernel_invariants('_num_bins', '_bin_width', '_bin_spacing')

    def run(self):
        # DAX init
        self.dax_init()

        if self._plot_trace:
            # Open applet
            self.time_resolved.context.plot()

        # Generated partition table based on number of bins
        self._partition_table_bins = self.time_resolved.partition_bins(self._num_bins, self._bin_width,
                                                                       self._bin_spacing)
        self.update_kernel_invariants('_partition_table_bins')

        # Run kernel
        self._run()

    @kernel
    def _run(self):
        # Reset core
        self.core.reset()

        # Switch off cooling and add a delay
        self.trap.cool_off()
        delay(100 * us)

        with self.time_resolved.context:
            for partition_num_bins, partition_offset in self._partition_table_bins:
                # Guarantee slack
                self.core.break_realtime()

                with parallel:
                    # Event of interest
                    self.trap.cool_pulse(200 * us)
                    # Detection
                    self.time_resolved.detect(0, partition_num_bins, self._bin_width, self._bin_spacing,
                                              offset=partition_offset)
                    # Delay the full time
                    delay(self._num_bins * (self._bin_width + self._bin_spacing))

                # Store results
                self.time_resolved.count(0, partition_num_bins)

        # Sync with core
        self.core.wait_until_mu(now_mu())

    def analyze(self):
        # Save trace as PDF file
        a = TimeResolvedAnalyzer(self)
        a.plot_all_traces()
