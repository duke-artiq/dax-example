"""
This file demonstrates how to use DAX.scheduler, a tool for automatic scheduling of experiments.
Users create a scheduling graph by defining jobs and triggers that inherit from the DAX.scheduler base classes.
All nodes are then added to the scheduler experiment that will appear in the ARTIQ dashboard as the scheduler frontend.
Finally, the scheduler client experiment is added which can be used to manually trigger the scheduler.
"""

from dax.scheduler import *


class LoadIons(Job):
    FILE = 'services/ion_load.py'
    CLASS_NAME = 'IonLoadExperiment'
    INTERVAL = '30s'

    def build_job(self) -> None:
        self.ARGUMENTS['Number of ions'] = self.get_argument('Number of ions',
                                                             NumberValue(2, min=1, max=4, step=1, ndecimals=0))


class StateExperiment(Job):
    FILE = 'services/state.py'
    CLASS_NAME = 'StateExperiment'
    ARGUMENTS = {
        'Num samples': 200,
        'Plot histogram': False,
    }
    DEPENDENCIES = {LoadIons}


class TimeResolvedExperiment(Job):
    FILE = 'services/time_resolved.py'
    CLASS_NAME = 'TimeResolvedExperiment'
    ARGUMENTS = {
        'Num bins': 256,
        'Plot trace': False,
    }
    DEPENDENCIES = {LoadIons}


class DetectionScan(Job):
    FILE = 'scan/detection_scan.py'
    CLASS_NAME = 'DetectionScan'
    ARGUMENTS = {
        'Detection time': RangeScan(5 * us, 50 * us, 45),
        'Plot probability': False,
        'Plot mean count': False,
        'Plot state probability': False,
    }
    DEPENDENCIES = {StateExperiment, TimeResolvedExperiment}


class DetectionScanTrigger(Trigger):
    NODES = [DetectionScan]
    POLICY = Policy.GREEDY
    INTERVAL = '1m'


class Scheduler(DaxScheduler, Experiment):
    NAME = 'scheduler'
    NODES = {LoadIons, StateExperiment, TimeResolvedExperiment, DetectionScan, DetectionScanTrigger}
    CONTROLLER = 'dax_scheduler'  # The definition of this controller can be found in the device DB
    DEFAULT_WAVE_INTERVAL = 5 * s


class SchedulerClient(dax_scheduler_client(Scheduler)):
    """Scheduler client"""
    pass
