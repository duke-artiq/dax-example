"""
This file demonstrates how to use DAX.scan, a tool to easily create scanning-type experiments.
DAX.scan can be used in conjunction with your existing system code as shown in the detection scan experiment.
"""

from dax.scan import DaxScan
from dax.modules.hist_context import HistogramAnalyzer

from dax_example.system import *


class DetectionScan(DaxScan, DaxExampleSystem, Experiment):
    """Detection scan"""

    T_KEY = 't'
    T_LABEL = 'Detection time'
    T_DATASET_KEY = 'plot.t_values'

    def build_scan(self) -> None:
        # Add scans
        self.add_scan(self.T_KEY, self.T_LABEL, Scannable(RangeScan(10 * us, 50 * us, 40),
                                                          unit='us', global_min=0 * us, global_step=10 * us))

        # Add normal arguments
        self._num_samples = self.get_argument('Num samples', NumberValue(100, min=1, step=1, ndecimals=0))
        self._plot_probability = self.get_argument('Plot probability', BooleanValue(True))
        self._plot_mean_count = self.get_argument('Plot mean count', BooleanValue(False))
        self._plot_state_probability = self.get_argument('Plot state probability', BooleanValue(True))
        self.update_kernel_invariants('_num_samples')

    def host_setup(self) -> None:
        # DAX init
        self.dax_init()

        # Broadcast t values for plotting
        self.set_dataset(self.T_DATASET_KEY, self.get_scannables()[self.T_KEY], broadcast=True, archive=False)

        # Open applets
        if self._plot_probability:
            self.state.context.plot_probability(x=self.T_DATASET_KEY, x_label=self.T_LABEL)
        if self._plot_mean_count:
            self.state.context.plot_mean_count(x=self.T_DATASET_KEY, x_label=self.T_LABEL)
        if self._plot_state_probability:
            self.state.context.plot_state_probability(x=self.T_DATASET_KEY, x_label=self.T_LABEL)

    @kernel
    def device_setup(self):  # type: () -> None
        # Reset core
        self.core.reset()

    @kernel
    def run_point(self, point, index):
        with self.state.context:
            for i in range(self._num_samples):
                # Guarantee slack
                self.core.break_realtime()
                # Cooling
                self.trap.cool_pulse()
                # Detection and count
                self.state.detect_active(point.t)
                self.state.count_active()

    @kernel
    def device_cleanup(self):  # type: () -> None
        # Ensure all events are submitted
        self.core.wait_until_mu(now_mu())

    def analyze(self):
        if not self.is_infinite_scan and not self.is_terminated_scan:
            # Save PDF plots
            x_values = self.get_scannables()[self.T_KEY]
            h = HistogramAnalyzer(self)
            h.plot_all_probabilities(x_values=x_values)
            h.plot_all_mean_counts(x_values=x_values)
            h.plot_all_state_probabilities(x_values=x_values)
