"""
This file demonstrates how to use DAX.servo, a tool to easily create servo-type experiments.
DAX.servo can be used in conjunction with your existing system code as shown in the detection servo experiment.
"""

from dax.servo import DaxServo

from dax_example.system import *


class DetectionServo(DaxServo, DaxExampleSystem, Experiment):
    """Detection servo"""

    SERVO_ITERATIONS_DEFAULT = 100

    T_KEY = 't'
    T_LABEL = 'Detection time'

    def build_servo(self) -> None:
        # Add servos
        self.add_servo_argument(self.T_KEY, self.T_LABEL, NumberValue(1 * us, unit='us', min=0.0))

        # Add normal arguments
        self._target_pmt = self.get_argument('Target PMT', NumberValue(0, min=0, max=DetectionModule.MAX_NUM_CHANNELS,
                                                                       step=1, ndecimals=0))
        self._threshold = self.get_argument('Threshold', NumberValue(1000, min=1, step=1, ndecimals=0))
        self._step_size = self.get_argument('Step size', NumberValue(1 * us, unit='us', min=0.0))
        self._plot_servo = self.get_argument('Plot servo', BooleanValue(True))
        self._normalize = self.get_argument('Normalize', BooleanValue(False))
        self.update_kernel_invariants('_target_pmt', '_threshold', '_step_size')

    def host_setup(self) -> None:
        # DAX init
        self.dax_init()

        # Open applets
        if self._plot_servo:
            self.plot_servo(normalize=self._normalize)

    @kernel
    def device_setup(self):  # type: () -> None
        # Reset core
        self.core.reset()

    @kernel
    def run_point(self, point, index):
        # Guarantee slack
        self.core.break_realtime()
        # Cooling
        self.trap.cool_pulse()
        # Detection and count
        self.detection.detect_channels([self._target_pmt], point.t)
        count = self.detection.count(self._target_pmt)

        # Check the threshold
        if count < self._threshold:
            # Increase the servo value with the step size
            point.t += self._step_size
        else:
            # Stop the servo when the threshold is met
            self.stop_servo()

    @kernel
    def device_cleanup(self):  # type: () -> None
        # Ensure all events are submitted
        self.core.wait_until_mu(now_mu())
