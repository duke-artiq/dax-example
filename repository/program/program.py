"""
DAX has a notion of clients which are abstract experiments that can be instantiated against a DAX system.

This file instantiates a program client, which can be used to dynamically link and run DAX.program files on
the example system. Example programs can be found in the ``programs`` directory of this project.
"""

import dax.clients.program

from dax_example.system import DaxExampleSystem


# noinspection PyTypeChecker
class ProgramClient(dax.clients.program.ProgramClient(DaxExampleSystem)):
    """Program client"""
    pass
