"""
The detection experiment demonstrates how the detection module can be used.
"""

import numpy as np

from dax_example.system import *


class DetectionExperiment(DaxExampleSystem, Experiment):
    """Detection module"""

    DATASET_KEY = 'count_data'

    def build(self):
        # Call super
        super(DetectionExperiment, self).build()

        # Get arguments
        self._num_samples = self.get_argument('Num samples', NumberValue(8, min=1, max=64, step=1, ndecimals=0))
        self._detection_time = self.get_argument('Detection time',
                                                 NumberValue(200 * us, unit='us', min=0 * us, step=10 * us))
        self._delay_time = self.get_argument('Delay time',
                                             NumberValue(10 * us, unit='us', min=0 * us, step=10 * us))
        self._detection_beam = self.get_argument('Detection beam', BooleanValue(True))
        self.update_kernel_invariants('_num_samples', '_detection_time', '_delay_time', '_detection_beam')

    def run(self):
        # Initialize system
        self.dax_init()

        # Prepare data storage
        self.set_dataset(self.DATASET_KEY, [])

        # Run kernel
        self._run()

        # Get data
        data = np.asarray(self.get_dataset(self.DATASET_KEY))
        self.logger.info(f'Collected data printed below:\n{data}')

    @kernel
    def _run(self):
        # Reset core
        self.core.reset()

        for _ in range(self._num_samples):
            # Add delay
            delay(self._delay_time)
            # Detection on active channels
            self.detection.detect_active(self._detection_time, detection_beam=self._detection_beam)

        for _ in range(self._num_samples):
            # Obtain all buffered counts
            counts = [self.detection.count(c) for c in self.detection.active_channels()]
            self.append_to_dataset(self.DATASET_KEY, counts)

        # Wait until all events have been submitted
        self.core.wait_until_mu(now_mu())
