{
  inputs = {
    artiq.url = "git+https://github.com/m-labs/artiq.git?ref=release-7";
    artiq-comtools.follows = "artiq/artiq-comtools";
    nixpkgs.follows = "artiq/nixpkgs";
    dax = {
      url = "git+https://gitlab.com/duke-artiq/dax.git";
      inputs = {
        artiqpkgs.follows = "artiq";
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "artiq/sipyco";
        flake8-artiq.follows = "flake8-artiq";
      };
    };
    dax-applets = {
      url = "git+https://gitlab.com/duke-artiq/dax-applets.git?ref=release-7";
      inputs = {
        artiqpkgs.follows = "artiq";
        nixpkgs.follows = "nixpkgs";
      };
    };
    dax-comtools = {
      url = "git+https://gitlab.com/duke-artiq/dax-comtools.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "artiq/sipyco";
      };
    };
    flake8-artiq = {
      url = "git+https://gitlab.com/duke-artiq/flake8-artiq.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs =
    { self, artiq, artiq-comtools, nixpkgs, dax, dax-applets, dax-comtools, flake8-artiq }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      artiqpkgs = artiq.packages.x86_64-linux // artiq-comtools.packages.x86_64-linux;
      daxpkgs = dax.packages.x86_64-linux // dax-applets.packages.x86_64-linux // dax-comtools.packages.x86_64-linux
        // flake8-artiq.packages.x86_64-linux;
    in
    {
      # Default shell for `nix develop`
      devShells.x86_64-linux.default = pkgs.mkShell {
        buildInputs = [
          # Python packages
          (pkgs.python3.withPackages (ps: [
            artiqpkgs.artiq
            artiqpkgs.artiq-comtools
            daxpkgs.dax
            daxpkgs.dax-applets
            daxpkgs.dax-comtools
            daxpkgs.flake8-artiq
            ps.pytest
            ps.autopep8
          ]))
        ];
      };
      # Enables use of `nix fmt`
      formatter.x86_64-linux = pkgs.nixpkgs-fmt;
    };

  # Settings to enable substitution from the M-Labs servers (avoiding local builds)
  nixConfig = {
    extra-trusted-public-keys = [ "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc=" ];
    extra-substituters = [ "https://nixbld.m-labs.hk" ];
  };
}
