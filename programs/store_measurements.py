"""
This program demonstrates how to store measurements using the data context.
"""

import numpy as np

from dax.program import *


class StoreMeasurements(DaxProgram, Experiment):

    @kernel
    def run(self):
        self.core.reset()

        with self.data_context:
            for _ in range(10):
                self.core.break_realtime()

                self.q.prep_0_all()
                with parallel:
                    self.q.h(0)
                    self.q.h(1)
                self.q.m_z_all()

                self.q.store_measurements_all()

    def analyze(self):
        data = np.asarray(self.data_context.get_raw())
        self.logger.info(f'Collected data:\n{data}')
