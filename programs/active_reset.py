"""
Active reset qubits to the |0> state.
"""

from dax.program import *


class ActiveReset(DaxProgram, Experiment):

    @kernel
    def run(self):
        self.core.reset()
        self.q.prep_0_all()
        for i in range(self.q.num_qubits):
            self.q.h(i)
        self.q.m_z_all()

        measurements = [self.q.get_measurement(i) for i in range(self.q.num_qubits)]

        self.core.break_realtime()
        for i in range(self.q.num_qubits):
            if measurements[i]:
                self.q.x(i)
        self.core.wait_until_mu(now_mu())
