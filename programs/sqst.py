"""
A program for single-qubit state tomography.

Author: Jacob Whitlow
"""

import numpy as np

from dax.program import *


class SQST(DaxProgram, Experiment):

    def build(self):
        # Get arguments
        self._target_qubit = self.get_argument("target_qubit",
                                               NumberValue(default=0, ndecimals=0, scale=1, step=1))
        self._num_steps_theta = self.get_argument("num_steps_theta",
                                                  NumberValue(default=5, ndecimals=0, scale=1, step=1, min=2))
        self._num_steps_phi = self.get_argument("num_steps_phi",
                                                NumberValue(default=10, ndecimals=0, scale=1, step=1, min=2))
        self._num_measurements = self.get_argument("num_measurements",
                                                   NumberValue(default=100, ndecimals=0, scale=1, step=1, min=1))

        # Kernel Invariants
        self.update_kernel_invariants("_target_qubit", "_num_measurements")

    def prepare(self):
        # Prepare data
        self._thetas = np.linspace(0.0, np.pi, num=self._num_steps_theta)
        self._phis = np.linspace(-np.pi, np.pi, num=self._num_steps_phi)
        self.update_kernel_invariants("_thetas", "_phis")

    @kernel
    def run(self):
        for theta in self._thetas:
            for phi in self._phis:
                # Run state tomography
                self.logger.info("theta=%f, phi=%f", theta, phi)
                self._sqst(theta, phi)

    @kernel
    def _sqst(self, theta, phi):
        with self.data_context:
            for basis in ["x", "y", "z"]:
                for _ in range(self._num_measurements):
                    self.core.reset()
                    self._create_state(theta, phi)
                    self._measure(basis)

    @kernel
    def _create_state(self, theta, phi):
        self.q.prep_0_all()
        self.q.ry(theta, self._target_qubit)
        self.q.rz(phi, self._target_qubit)

    @kernel
    def _measure(self, basis):
        if basis == "x":  # Change to X basis
            self.q.h(self._target_qubit)
        elif basis == "y":  # Change to Y basis
            self.q.sqrt_z_dag(self._target_qubit)
            self.q.h(self._target_qubit)
            self.q.sqrt_z(self._target_qubit)

        self.q.m_z_all()  # Measure in Z basis
        self.q.store_measurements([self._target_qubit])

    def analyze(self):
        # Acquire and process data
        data = np.asarray(self.data_context.get_raw())

        for d, (theta, phi) in zip(data, ((t, p) for t in self._thetas for p in self._phis)):
            # Gather Data
            x_data, y_data, z_data = np.split(d, 3)

            # Based on (nx, ny, nz) = ((nx0 - nx1)/(nx0 + nx1), (ny0 - ny1)/(ny0 + ny1), (nz0 - nz1)/(nz0 + nz1))
            nx = (self._num_measurements - 2 * np.count_nonzero(x_data)) / self._num_measurements
            ny = (self._num_measurements - 2 * np.count_nonzero(y_data)) / self._num_measurements
            nz = (self._num_measurements - 2 * np.count_nonzero(z_data)) / self._num_measurements

            # Convert to measured (r, theta, phi)
            r_avg = np.sqrt(nx ** 2 + ny ** 2 + nz ** 2)
            theta_avg = np.arccos(nz / r_avg) / np.pi
            phi_avg = np.int32(ny / np.abs(ny)) * np.arccos(nx / np.sqrt(nx ** 2 + ny ** 2)) / np.pi

            # Calculate fidelity with desired state
            fidelity = nx * np.sin(theta) * np.cos(phi) + ny * np.sin(theta) * np.sin(phi) + nz * np.cos(theta) + 1
            fidelity /= 2

            self.logger.info(f'Desired State: (theta, phi) = ({theta}, {phi})')
            self.logger.debug(f'Quantum State: (nx, ny, nz) = ({nx}, {ny}, {nz})')
            self.logger.debug(f'Quantum State: (r, theta, phi) = ({r_avg}, {theta_avg}, {phi_avg}')
            self.logger.info(f'Fidelity with desired state = {fidelity}')
