"""
A program demonstrating various available gates.
"""

from dax.program import *


class Gates(DaxProgram, Experiment):

    @kernel
    def run(self):
        self.core.reset()
        self.q.prep_0_all()

        self.q.i(0)
        self.q.x(0)
        self.q.y(0)
        self.q.z(0)
        self.q.h(0)

        self.q.sqrt_x(0)
        self.q.sqrt_x_dag(0)
        self.q.sqrt_y(0)
        self.q.sqrt_y_dag(0)
        self.q.sqrt_z(0)
        self.q.sqrt_z_dag(0)

        self.q.rx(self.q.pi / 4, 0)
        self.q.ry(self.q.pi / 4, 0)
        self.q.rz(self.q.pi / 4, 0)

        self.core.wait_until_mu(now_mu())
