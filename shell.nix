{ pkgs ? import <nixpkgs> { }
, artiqpkgs ? import <artiq-full> { inherit pkgs; }
}:

let
  daxVersion = "7.3";
  daxSrc = builtins.fetchGit {
    url = "https://gitlab.com/duke-artiq/dax.git";
    ref = "refs/tags/v${daxVersion}";
  };
  dax = pkgs.callPackage (import daxSrc) { inherit pkgs artiqpkgs daxVersion; };
  dax-applets = pkgs.callPackage
    (import (builtins.fetchGit "https://gitlab.com/duke-artiq/dax-applets.git"))
    { inherit pkgs artiqpkgs; };
  flake8-artiq = pkgs.callPackage
    (import (builtins.fetchGit {
      url = "https://gitlab.com/duke-artiq/flake8-artiq.git";
      ref = "refs/tags/v0.1.0";
    }))
    { inherit pkgs; };

in
pkgs.mkShell {
  buildInputs = [
    # Python packages
    (pkgs.python3.withPackages (ps: [
      # ARTIQ
      artiqpkgs.artiq
      artiqpkgs.artiq-comtools
      # DAX
      dax
      dax-applets
      # Testing packages
      ps.pytest
      ps.flake8
      flake8-artiq
    ]))
  ];
}
